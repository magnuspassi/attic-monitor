import time
from machine import I2C, Pin, ADC
import ccs811
from dht import DTH
import json
import pycom
from mqtt_client import MQTTClient

pycom.heartbeat(False)

with open('config.json', 'r') as f:
    data = json.load(f)
f.close()

for mqtt in data['mqtt']:
    mqtt_id = mqtt['id']
    broker_ip = mqtt['broker_ip']
    broker_port = int(mqtt['port'])
    mqtt_topic = mqtt['topic']

def blink_led():
    for n in range(1):
        pycom.rgbled(0x220022)
        time.sleep(0.3)
        pycom.rgbled(0x000000)
        time.sleep(0.2)

# battery voltage
adc = ADC()
bat_voltage = adc.channel(attn=ADC.ATTN_11DB, pin='P16')

# establishes connection to MQTT broker
client = MQTTClient(mqtt_id, broker_ip, broker_port, keepalive=300)
client.connect()

# CCS811 configuration
i2c = I2C(0, I2C.MASTER, baudrate=100000)
ccs = ccs811.CCS811(i2c, addr=90)

# DHT11 configuration
th = DTH(Pin('P23', mode=Pin.OPEN_DRAIN), 0)
time.sleep(2)

while(True):
    result = th.read()
    while not result.is_valid():
        time.sleep(.5)
        result = th.read()
    ccs.data_ready()
    time.sleep(5)
    co2 = ccs.eCO2
    tvoc = ccs.tVOC
    temp = result.temperature
    hum = result.humidity
    bat_percent = ((bat_voltage.voltage()*2)-3200)/10
    if bat_percent > 100:
        bat_percent = 100
    print('Sending values...')
    client.publish(mqtt_topic, '{"Attic Monitor": {  "co2":' + str(co2) +
                                                    ',"tvoc":' + str(tvoc) +
                                                    ',"temp":' + str(temp) +
                                                    ',"hum":' + str(hum) +
                                                    ',"batt":' + str(bat_percent) +
                                                    '}}')
    blink_led()
    print('Values sent successfully!')
    time.sleep(300)
