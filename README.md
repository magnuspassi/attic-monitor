###### Tags: `IoT` `Lopy4` `Pycom` `MQTT` `Micropython` `TIG Stack`

# Attic Monitor

Project for the LNU Applied IoT course

## Description
A IoT device to monitor air quality, humidity and more.

## Project status
All the necessary sensors are bought and a local TIG-stack is deployed with MQTT configured.

## Todo
- Clean up code
