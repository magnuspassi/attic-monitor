import json
import machine
import pycom
from network import WLAN

pycom.pybytes_on_boot(False)

with open('config.json', 'r') as f:
    data = json.load(f)
f.close()

for wifi in data['wifi']:
    ssid = wifi['ssid']
    wifi_passwd = wifi['passwd']

wlan = WLAN(mode=WLAN.STA)
wlan.connect(ssid, auth=(WLAN.WPA2, wifi_passwd), timeout=5000)

while not wlan.isconnected():
    machine.idle()
print("Connected to Wifi\n")
